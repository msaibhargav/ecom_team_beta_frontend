import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login-register/login/login.component';
import { RegisterComponent } from './login-register/register/register.component';
import { HeaderComponent } from './common-components/header/header.component';
import { AddproductComponent } from './products/addproduct/addproduct.component';
import { ViewprodutsComponent } from './products/viewproduts/viewproduts.component';
import { VieworderComponent } from './orders/vieworder/vieworder.component';
import { ViewallordersComponent } from './orders/viewallorders/viewallorders.component';
import { AddcategoryComponent } from './category/addcategory/addcategory.component';
import { MakepaymentComponent } from './payment/makepayment/makepayment.component';
import {EcomInterceptorInterceptor} from 'src/app/ecom-interceptor.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    AddproductComponent,
    ViewprodutsComponent,
    VieworderComponent,
    ViewallordersComponent,
    AddcategoryComponent,
    MakepaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,

      useClass: EcomInterceptorInterceptor,

      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
