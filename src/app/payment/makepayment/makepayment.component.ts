import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServicesService } from 'src/services/services.service';
import { DataService } from 'src/services/data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-makepayment',
  templateUrl: './makepayment.component.html',
  styleUrls: ['./makepayment.component.scss']
})
export class MakepaymentComponent implements OnInit {

  constructor(private formbuilder: FormBuilder, private api: ServicesService,private dataservice:DataService,private router:Router) { }
  productToOrder:any;
  quantity=1;
  price:any;
  user:any;
  
  ngOnInit(): void {
    this.productToOrder=this.dataservice.getObject();
    this.price=this.productToOrder.productPrice;
    this.user = JSON.parse(localStorage.getItem('User')||"{}")
    console.log(this.productToOrder);
    console.log(this.user);
    console.log('in makepayment')
  }  


  PaymentForm: FormGroup = this.formbuilder.group({
    cardnumber:['', [Validators.required]],
    cardCvv:['', [Validators.required]],
    expdate:['', [Validators.required]],
    name:['', [Validators.required]],
    paymentmode:['', [Validators.required]],
    test:['', [Validators.required]],

  })
  placeOrderandPayment(){
    if(this.PaymentForm.status=='VALID'){
      // {
      //   "productId": 0,
      //   "customerId": 0,
      //   "orderQuantity": 0,
      //   "orderPrice": 0,
      //   "shipmentAddress": "string"
      // }
      let Order = {
        productId:this.productToOrder.productId,
        customerId:this.user.customerId,
        orderQuantity:this.quantity,
        orderPrice: this.price,
        shipmentAddress:this.user.address
      } 
      console.log(typeof(Order))
      console.log("this is order ")
      console.log(Order);


      let orderUrl = `https://localhost:44390/api/ApiGateway/place-order`;
      this.api.post(orderUrl,Order).subscribe(res=>{
        console.log("Order created",res)
        let orderResult = JSON.parse(JSON.stringify(res));
        console.log("order placed")
        console.log(orderResult);
        localStorage.setItem('Order', JSON.stringify(orderResult));
        let makePaymentUrl = 'https://localhost:44390/api/ApiGateway/make-payment';
        let body = {
          orderId: orderResult.orderId,
          customerId: this.user.customerId,
          paymentMode: this.PaymentForm.value.paymentmode,
          cardNumber: this.PaymentForm.value.cardnumber,
          cardCvv: this.PaymentForm.value.cardCvv,
          cardExpiry: this.PaymentForm.value.expdate,
          cardName: this.PaymentForm.value.name
        }
        console.log(body);
      this.api.post(makePaymentUrl,body).subscribe(res=>{
        console.log(res);
        alert("Payment Made Successfully!")
        this.router.navigateByUrl('app-vieworder');
      })
      })
    }else{
      alert("Error While Making Payment!")
      console.log("Not Working!!")
    }
  }
  quannchange(){
    this.price = this.productToOrder.productPrice*this.quantity;
  }

}
