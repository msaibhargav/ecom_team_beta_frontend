import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vieworder',
  templateUrl: './vieworder.component.html',
  styleUrls: ['./vieworder.component.scss']
})
export class VieworderComponent implements OnInit {

  constructor(private router:Router) { }
  user:any;
  product:any
  body:any;
  order:any;

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('User')||"{}")
    this.product = JSON.parse(localStorage.getItem('Product')||"{}")
    this.order = JSON.parse(localStorage.getItem('Order')||"{}")
    this.getOrders()
    console.log(this.user);
    console.log(this.product);
  }
getOrders(){
  console.log(this.product.productName);
  console.log(this.user.orderPrice);
  console.log(this.user.orderQuantity);
  console.log(this.user.address);
  this.body={
    productName:this.product.productName,
    productPrice:this.order.orderPrice,
    orderQuantity:this.order.orderQuantity,
    shipmentAddress:this.user.address,
    status:"Order Placed"
  }
  console.log("END!!")
}
}
