import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './login-register/register/register.component';
import { LoginComponent } from './login-register/login/login.component';
import {HeaderComponent} from './common-components/header/header.component';
import { ViewprodutsComponent } from './products/viewproduts/viewproduts.component';
import { AddproductComponent } from './products/addproduct/addproduct.component';
import { AddcategoryComponent } from './category/addcategory/addcategory.component';
import { ViewallordersComponent } from './orders/viewallorders/viewallorders.component';
import { VieworderComponent } from './orders/vieworder/vieworder.component';
import { MakepaymentComponent } from './payment/makepayment/makepayment.component';

const routes: Routes = [
  {path:"app-register",component:RegisterComponent},
  {path:"app-login",component:LoginComponent},
  {path:"app-header",component:HeaderComponent},
  {path:"app-viewproduts",component:ViewprodutsComponent},
  {path:"app-addprodut",component:AddproductComponent},
  {path:"app-viewcategory",component:AddcategoryComponent},
  {path:"app-viewallorders",component:ViewallordersComponent},
  {path:"app-vieworder",component:VieworderComponent},
  {path:"app-addproduct",component:AddproductComponent},
  {path:"app-addcategory",component:AddcategoryComponent},
  {path:"app-makepayment",component:MakepaymentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
