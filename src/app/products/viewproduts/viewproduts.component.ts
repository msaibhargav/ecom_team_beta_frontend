import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServicesService } from 'src/services/services.service';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-viewproduts',
  templateUrl: './viewproduts.component.html',
  styleUrls: ['./viewproduts.component.scss']
})
export class ViewprodutsComponent implements OnInit {

  constructor(private formbuilder: FormBuilder, private api: ServicesService,private router:Router,private dataservice:DataService) { }
  products:any;
  categories:any;
  ngOnInit(): void {
    this.getAllProducts();
    this.getAllCategory();
  }
  myCatForm: FormGroup = this.formbuilder.group({
    mySelect:['', [Validators.required]],

  })
  trackByIndex = (index:number):number =>{
    return index;
  }
getAllProducts(){
  this.api.get("https://localhost:44390/api/ApiGateway/get-all-products").subscribe(res=>{
    this.products=res;
    console.log(this.products)
  })
}
getAllCategory(){
  this.api.get("https://localhost:44390/api/ApiGateway/get-all-category").subscribe(res=>{
    this.categories=res;
    console.log(this.categories)
  })
}

getProductToPlaceOrder(index:number)
{
  var seletedProduct = this.products[index];
  localStorage.setItem('Product', JSON.stringify(seletedProduct));
  this.dataservice.setObject(this.products[index])
  console.log(this.products[index]);
  this.router.navigateByUrl('app-makepayment');
}
filter(){
  if(this.myCatForm.value.mySelect=="All"){
    this.products=this.getAllProducts()
  }
  else{
    let url='https://localhost:44390/api/ApiGateway/get-product-by-cat-id?id='+this.myCatForm.value.mySelect;
    this.api.post(url).subscribe(res=>{
      console.log(res)
      this.products = res;
    })

  }
}
}
